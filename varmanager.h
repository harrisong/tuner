#include <cstring>
#include <map>
#include <cstdint>
#include "myspinbox.h"
#pragma once

class VarManager{
public:
    VarManager();
    void AddVariable(int id, MySpinBox* box);
    void RemoveVariable(int id);
    MySpinBox* GetVariable(int id);
    QString toString();
    std::map<int,MySpinBox*>& GetList(){
      return m_list;
    }

private:
    std::map<int,MySpinBox*> m_list;
};
