#include "varmanager.h"
#include <cstdint>
#include <algorithm>
#include <iterator>
#include <map>
#include <QString>
#include <QDebug>
VarManager::VarManager()
{

}

void VarManager::AddVariable(int id, MySpinBox* box){
  m_list.insert(std::pair<int,MySpinBox*>(id,box));
}

void VarManager::RemoveVariable(int id){
    m_list.erase(id);
}

MySpinBox* VarManager::GetVariable(int id){
    return m_list.at(id);
}

QString VarManager::toString(){
  typedef std::map<int,MySpinBox*>::iterator it_type;
  QString str = "";

  for(it_type iterator = m_list.begin(); iterator != m_list.end(); iterator++) {
      QString vartype = (iterator->second->objectName()=="MySpinBox") ? "int" : "real";

      if(iterator->second->objectName()=="MySpinBox"){
          str += iterator->second->name + ",int," + QString::number(iterator->second->id) + ",";
          MySpinBox* spin = static_cast<MySpinBox*>(iterator->second);
          str += QString::number(spin->value());

      }else{
          str += iterator->second->name + ",real," + QString::number(iterator->second->id) + ",";
          MyDoubleSpinBox* spin = reinterpret_cast<MyDoubleSpinBox*>(iterator->second);
          str += QString::number(spin->value());
      }

      str += "\n";
  }
  return str;
}
