#include "tuner.h"
#include "myspinbox.h"
#include <QVBoxLayout>
#include <QSpinBox>
#include <QDebug>
#include <QFileDialog>
#include <QDir>
#include <QMessageBox>

#include <iostream>
#include <sstream>
#include <iomanip>

using namespace std;

Tuner::Tuner(QWidget *parent)
{
  setWindowTitle("Tuning");
  resize(200,200);

  layout.setAlignment(Qt::AlignTop);

  QHBoxLayout* filegroup = new QHBoxLayout;

  QPushButton* open = new QPushButton;
  open->setText("Load");
  filegroup->addWidget(open);

  QPushButton* save = new QPushButton;
  save->setText("Save");
  filegroup->addWidget(save);

  QPushButton* sendall = new QPushButton;
  sendall->setText("Send All");
  filegroup->addWidget(sendall);

  layout.addLayout(filegroup);

  connect(open,SIGNAL(clicked()), this, SLOT(openTuning()));
  connect(save,SIGNAL(clicked()), this, SLOT(saveTuning()));
  connect(sendall,SIGNAL(clicked()), this, SLOT(sendAll()));

  setLayout(&layout);

}

Tuner::~Tuner(){

}

void Tuner::CreateVar(int id, uint32_t val, QString name){
  if(!CheckExists(id)){
    QHBoxLayout *grouplayout = new QHBoxLayout;
    QLabel *label = new QLabel(name);
    label->adjustSize();
    grouplayout->addWidget(label);

    MySpinBox* spin = new MySpinBox;
    spin->id = id;
    spin->name = name;
    spin->setRange(-999999, 999999);
    spin->setSingleStep(1);
    spin->setValue(val);

    grouplayout->addWidget(spin);
    layout.addLayout(grouplayout);
    setLayout(&layout);

    m_varmanager.AddVariable(id, spin);


    connect(spin,SIGNAL(valueChanged(const QString&)),this,SLOT(spinboxHandler()));
  }
}



void Tuner::CreateVar(int id, float val, QString name){
  if(!CheckExists(id)){
    QHBoxLayout *grouplayout = new QHBoxLayout;
    QLabel *label = new QLabel(name);
    label->adjustSize();
    grouplayout->addWidget(label);

    MyDoubleSpinBox* spin = new MyDoubleSpinBox;
    spin->id = id;
    spin->name = name;
    spin->setRange(-999999.0, 999999.0);
    spin->setDecimals(10);
    spin->setSingleStep(0.001);
    spin->setValue(val);

    grouplayout->addWidget(spin);
    layout.addLayout(grouplayout);
    setLayout(&layout);

    m_varmanager.AddVariable(id, reinterpret_cast<MySpinBox*>(spin));
    connect(spin,SIGNAL(valueChanged(const QString&)),this,SLOT(spinboxHandler()));
  }


}


void Tuner::DeleteVar(int id){
  MySpinBox* spin = m_varmanager.GetVariable(id);
  m_varmanager.RemoveVariable(id);
  delete spin;

}

void Tuner::spinboxHandler(){
  sendVar(QObject::sender());
}

void Tuner::sendVar(QObject* var){
  if(var->objectName()=="MySpinBox"){
      MySpinBox* spinbox = dynamic_cast<MySpinBox*>(var);
      uint32_t val = (uint32_t)spinbox->value();
      int id = spinbox->id;
      qDebug() << "Sent:" << (uint8_t) id << ((val & 0xFF000000) >> 24) << ((val & 0xFF0000) >> 16) << ((val & 0xFF00) >> 8) << (val & 0xFF);
      m_serial->sendByte(id);
      m_serial->sendByte((val & 0xFF000000) >> 24);
      m_serial->sendByte((val & 0xFF0000) >> 16);
      m_serial->sendByte((val & 0xFF00) >> 8);
      m_serial->sendByte(val & 0xFF);
  }else if(var->objectName()=="MyDoubleSpinBox"){
      MyDoubleSpinBox* spinbox = dynamic_cast<MyDoubleSpinBox*>(var);
      float value = (float)spinbox->value();
      unsigned char * val = reinterpret_cast<unsigned char *>(&value);
      int id = spinbox->id;
      qDebug() << "Sent:" << (uint8_t) id << (val[0]) << (val[1]) << (val[2]) << (val[3]);
      m_serial->sendByte(id);
      m_serial->sendByte(val[3]);
      m_serial->sendByte(val[2]);
      m_serial->sendByte(val[1]);
      m_serial->sendByte(val[0]);
  }
}

bool Tuner::CheckExists(int id){
  std::map<int,MySpinBox*> list = m_varmanager.GetList();
  typedef std::map<int,MySpinBox*>::iterator it_type;

  for(it_type iterator = list.begin(); iterator != list.end(); iterator++) {
      if(iterator->second->id == id){
          return true;
      }
  }
  return false;
}

void Tuner::SetSerial(SerialApp* serial){
  m_serial = serial;
}

void Tuner::toogleVisibility()
{
    SerialApp* serialapp = dynamic_cast<SerialApp*>(sender());
    if(this->isVisible()){
        this->setVisible(false);
        serialapp->getTunerButton()->setText("Show Tuner");
    }
    else{
        this->setVisible(true);
        serialapp->getTunerButton()->setText("Hide Tuner");
    }
}

void Tuner::createVar(QByteArray dataBytes){
    //qDebug() << dataBytes;

    if(dataBytes[0]=='L'){
        FILE *fp;

        stringstream hex_str_s;
        for (int i = 0; i < 128; ++i)
        {
          hex_str_s << std::setw(2) << std::setfill('0') << std::hex << (int)(uint8_t)dataBytes[i + 1];
        }

        /* Open the command for reading. */
        fp = popen(("/home/harrison/uartplot-1.0/bin/app1 " + hex_str_s.str()).c_str(), "r");
        if (fp == NULL) {
          printf("Failed to run command\n" );
    //      exit(1);
          return;
        }

        /* Read the output a line at a time - output it. */
        string str;
        for (char ch = fgetc(fp); ch != EOF; ch = fgetc(fp)) {
            str += ch;
        }
        FILE *sm = fopen("sm", "a");
        fwrite(str.c_str(), 1, str.size(), sm);
        fflush(sm);
        fclose(sm);
        qDebug() << str.c_str();

        pclose(fp);
    }
    else {
        QString str = static_cast<QString>(dataBytes);
        createVar(str);
      }

}

void Tuner::createVar(QString str){
  QStringList strlist = str.split(",");

//  qDebug() << "size:" << strlist.size();

  if(strlist.size()==4){
    qDebug() << strlist[0] << strlist[1] << strlist[2].toInt() << strlist[3].toFloat();
    if(strlist[1].compare("int") == 0){
      CreateVar(strlist[2].toInt(),static_cast<uint32_t>(strlist[3].toInt()),strlist[0]);
    }else if(strlist[1].compare("real") == 0){
      CreateVar(strlist[2].toInt(),strlist[3].toFloat(),strlist[0]);
    }
  }
}

void Tuner::saveTuning()
{
    QFileDialog dialog;
    QString selected = tr("Text Files (*.txt)");
    QString fileName = dialog.getSaveFileName(this,tr("Save File"),QDir::homePath(),tr("Text Files (*.txt);;CSV (*csv)"), &selected);

    if(fileName.isEmpty())
        return;

    QFile saveFile(fileName);
    if(!saveFile.open(QIODevice::WriteOnly))
    {
        QMessageBox::warning(this,"Error in saving","Unable to save the file.\nPlease check file permissions.");
        return;
    }

    QTextStream stream(&saveFile);
    stream << this->m_varmanager.toString();
    saveFile.close();
}

void Tuner::openTuning()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open File",QDir::homePath(),"Text Files (*.txt) ;; CSV (*csv)");

    if(fileName.isEmpty())
        return;

    QFile openFile(fileName);

    if (!openFile.open(QIODevice::ReadOnly | QIODevice::Text))
            return;

    QTextStream stream(&openFile);
    while (!stream.atEnd()) {
        QString line = stream.readLine();
        createVar(line);
    }

    openFile.close();
}

void Tuner::sendAll(){
  std::map<int,MySpinBox*> list = m_varmanager.GetList();
  typedef std::map<int,MySpinBox*>::iterator it_type;

  for(it_type iterator = list.begin(); iterator != list.end(); iterator++) {
      sendVar(iterator->second);
  }

}
