#pragma once
#include <QWindow>
#include <QWidget>
#include <QBoxLayout>
#include "varmanager.h"
#include "SerialApp.h"
#include <QDebug>

class Tuner : public QWidget{
  Q_OBJECT
public:
    explicit Tuner(QWidget *parent = 0);
    ~Tuner();
    void CreateVar(int id, uint32_t val, QString label);
    void CreateVar(int id, float val, QString name);
    void DeleteVar(int id);
    void SetSerial(SerialApp* serial);
    void sendVar(QObject* var);
    bool CheckExists(int id);


private:
    VarManager m_varmanager;
    QVBoxLayout layout;
    SerialApp* m_serial;

public slots:
    void toogleVisibility();
    void createVar(QString str);
    void createVar(QByteArray dataBytes);
    void saveTuning();
    void openTuning();

private slots:
    void spinboxHandler();
    void sendAll();


};
