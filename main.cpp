#include <QApplication>
#include "graphwidget.h"
#include "SerialApp.h"
#include "tuner.h"
#include "carpath.h"

#include <QGraphicsScene>
#include <QGraphicsView>
#include <QPainterPath>
#include <QPainter>

int main(int argc, char *argv[])
{
//    system("sudo rfcomm connect rfcomm0&");
    QApplication a(argc, argv);

    GraphWidget graphWidget;
    SerialApp serialWidget;
    serialWidget.show();

    graphWidget.hide();


    Tuner tunerWidget;
    tunerWidget.show();
    tunerWidget.SetSerial(&serialWidget);


    QObject::connect(&serialWidget,SIGNAL(showPlotButtonSignal()),&graphWidget,SLOT(toggleVisibility()));
    QObject::connect(&graphWidget,SIGNAL(maximizeButtonSignal()),&serialWidget,SLOT(toogleVisibility()));
    QObject::connect(&serialWidget.port,SIGNAL(lineReceived(QByteArray)),
                    &graphWidget,SLOT(lineReceiveSlot(QByteArray)));
    QObject::connect(&serialWidget,SIGNAL(lineReceivedApp(QByteArray)),&tunerWidget,SLOT(createVar(QByteArray)));
    QObject::connect(&serialWidget,SIGNAL(closePortSignal()),&graphWidget,SLOT(stopButtonSlot()));
    QObject::connect(&serialWidget,SIGNAL(toggleTunerSignal()),&tunerWidget,SLOT(toogleVisibility()));
    QObject::connect(&serialWidget,SIGNAL(closeSignal()),&a,SLOT(quit()));


    return a.exec();
}
