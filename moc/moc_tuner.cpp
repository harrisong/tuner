/****************************************************************************
** Meta object code from reading C++ file 'tuner.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../tuner.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'tuner.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_Tuner_t {
    QByteArrayData data[10];
    char stringdata[94];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_Tuner_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_Tuner_t qt_meta_stringdata_Tuner = {
    {
QT_MOC_LITERAL(0, 0, 5),
QT_MOC_LITERAL(1, 6, 16),
QT_MOC_LITERAL(2, 23, 0),
QT_MOC_LITERAL(3, 24, 9),
QT_MOC_LITERAL(4, 34, 3),
QT_MOC_LITERAL(5, 38, 9),
QT_MOC_LITERAL(6, 48, 10),
QT_MOC_LITERAL(7, 59, 10),
QT_MOC_LITERAL(8, 70, 14),
QT_MOC_LITERAL(9, 85, 7)
    },
    "Tuner\0toogleVisibility\0\0createVar\0str\0"
    "dataBytes\0saveTuning\0openTuning\0"
    "spinboxHandler\0sendAll\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_Tuner[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: name, argc, parameters, tag, flags
       1,    0,   49,    2, 0x0a,
       3,    1,   50,    2, 0x0a,
       3,    1,   53,    2, 0x0a,
       6,    0,   56,    2, 0x0a,
       7,    0,   57,    2, 0x0a,
       8,    0,   58,    2, 0x08,
       9,    0,   59,    2, 0x08,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QString,    4,
    QMetaType::Void, QMetaType::QByteArray,    5,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

       0        // eod
};

void Tuner::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Tuner *_t = static_cast<Tuner *>(_o);
        switch (_id) {
        case 0: _t->toogleVisibility(); break;
        case 1: _t->createVar((*reinterpret_cast< QString(*)>(_a[1]))); break;
        case 2: _t->createVar((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 3: _t->saveTuning(); break;
        case 4: _t->openTuning(); break;
        case 5: _t->spinboxHandler(); break;
        case 6: _t->sendAll(); break;
        default: ;
        }
    }
}

const QMetaObject Tuner::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_Tuner.data,
      qt_meta_data_Tuner,  qt_static_metacall, 0, 0}
};


const QMetaObject *Tuner::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *Tuner::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Tuner.stringdata))
        return static_cast<void*>(const_cast< Tuner*>(this));
    return QWidget::qt_metacast(_clname);
}

int Tuner::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 7)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 7;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
