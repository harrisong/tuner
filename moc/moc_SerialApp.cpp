/****************************************************************************
** Meta object code from reading C++ file 'SerialApp.h'
**
** Created by: The Qt Meta Object Compiler version 67 (Qt 5.2.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../SerialApp.h"
#include <QtCore/qbytearray.h>
#include <QtCore/qmetatype.h>
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'SerialApp.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 67
#error "This file was generated using the moc from 5.2.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
struct qt_meta_stringdata_SerialApp_t {
    QByteArrayData data[21];
    char stringdata[252];
};
#define QT_MOC_LITERAL(idx, ofs, len) \
    Q_STATIC_BYTE_ARRAY_DATA_HEADER_INITIALIZER_WITH_OFFSET(len, \
    offsetof(qt_meta_stringdata_SerialApp_t, stringdata) + ofs \
        - idx * sizeof(QByteArrayData) \
    )
static const qt_meta_stringdata_SerialApp_t qt_meta_stringdata_SerialApp = {
    {
QT_MOC_LITERAL(0, 0, 9),
QT_MOC_LITERAL(1, 10, 20),
QT_MOC_LITERAL(2, 31, 0),
QT_MOC_LITERAL(3, 32, 15),
QT_MOC_LITERAL(4, 48, 15),
QT_MOC_LITERAL(5, 64, 17),
QT_MOC_LITERAL(6, 82, 11),
QT_MOC_LITERAL(7, 94, 13),
QT_MOC_LITERAL(8, 108, 13),
QT_MOC_LITERAL(9, 122, 4),
QT_MOC_LITERAL(10, 127, 15),
QT_MOC_LITERAL(11, 143, 4),
QT_MOC_LITERAL(12, 148, 12),
QT_MOC_LITERAL(13, 161, 5),
QT_MOC_LITERAL(14, 167, 5),
QT_MOC_LITERAL(15, 173, 14),
QT_MOC_LITERAL(16, 188, 4),
QT_MOC_LITERAL(17, 193, 18),
QT_MOC_LITERAL(18, 212, 15),
QT_MOC_LITERAL(19, 228, 16),
QT_MOC_LITERAL(20, 245, 5)
    },
    "SerialApp\0showPlotButtonSignal\0\0"
    "lineReceivedApp\0closePortSignal\0"
    "toggleTunerSignal\0closeSignal\0"
    "releaseRfcomm\0connectRfcomm\0open\0"
    "closeConnection\0send\0dataReceived\0"
    "array\0clear\0refreshDevices\0save\0"
    "showPlotButtonSlot\0toggleTunerSlot\0"
    "toogleVisibility\0close\0"
};
#undef QT_MOC_LITERAL

static const uint qt_meta_data_SerialApp[] = {

 // content:
       7,       // revision
       0,       // classname
       0,    0, // classinfo
      18,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       5,       // signalCount

 // signals: name, argc, parameters, tag, flags
       1,    0,  104,    2, 0x06,
       3,    1,  105,    2, 0x06,
       4,    0,  108,    2, 0x06,
       5,    0,  109,    2, 0x06,
       6,    0,  110,    2, 0x06,

 // slots: name, argc, parameters, tag, flags
       7,    0,  111,    2, 0x0a,
       8,    0,  112,    2, 0x0a,
       9,    0,  113,    2, 0x0a,
      10,    0,  114,    2, 0x0a,
      11,    0,  115,    2, 0x0a,
      12,    1,  116,    2, 0x0a,
      14,    0,  119,    2, 0x0a,
      15,    0,  120,    2, 0x0a,
      16,    0,  121,    2, 0x0a,
      17,    0,  122,    2, 0x0a,
      18,    0,  123,    2, 0x0a,
      19,    0,  124,    2, 0x0a,
      20,    0,  125,    2, 0x0a,

 // signals: parameters
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,    2,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,

 // slots: parameters
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void, QMetaType::QByteArray,   13,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Void,
    QMetaType::Bool,

       0        // eod
};

void SerialApp::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        SerialApp *_t = static_cast<SerialApp *>(_o);
        switch (_id) {
        case 0: _t->showPlotButtonSignal(); break;
        case 1: _t->lineReceivedApp((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 2: _t->closePortSignal(); break;
        case 3: _t->toggleTunerSignal(); break;
        case 4: _t->closeSignal(); break;
        case 5: _t->releaseRfcomm(); break;
        case 6: _t->connectRfcomm(); break;
        case 7: _t->open(); break;
        case 8: _t->closeConnection(); break;
        case 9: _t->send(); break;
        case 10: _t->dataReceived((*reinterpret_cast< QByteArray(*)>(_a[1]))); break;
        case 11: _t->clear(); break;
        case 12: _t->refreshDevices(); break;
        case 13: _t->save(); break;
        case 14: _t->showPlotButtonSlot(); break;
        case 15: _t->toggleTunerSlot(); break;
        case 16: _t->toogleVisibility(); break;
        case 17: { bool _r = _t->close();
            if (_a[0]) *reinterpret_cast< bool*>(_a[0]) = _r; }  break;
        default: ;
        }
    } else if (_c == QMetaObject::IndexOfMethod) {
        int *result = reinterpret_cast<int *>(_a[0]);
        void **func = reinterpret_cast<void **>(_a[1]);
        {
            typedef void (SerialApp::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialApp::showPlotButtonSignal)) {
                *result = 0;
            }
        }
        {
            typedef void (SerialApp::*_t)(QByteArray );
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialApp::lineReceivedApp)) {
                *result = 1;
            }
        }
        {
            typedef void (SerialApp::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialApp::closePortSignal)) {
                *result = 2;
            }
        }
        {
            typedef void (SerialApp::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialApp::toggleTunerSignal)) {
                *result = 3;
            }
        }
        {
            typedef void (SerialApp::*_t)();
            if (*reinterpret_cast<_t *>(func) == static_cast<_t>(&SerialApp::closeSignal)) {
                *result = 4;
            }
        }
    }
}

const QMetaObject SerialApp::staticMetaObject = {
    { &QWidget::staticMetaObject, qt_meta_stringdata_SerialApp.data,
      qt_meta_data_SerialApp,  qt_static_metacall, 0, 0}
};


const QMetaObject *SerialApp::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->dynamicMetaObject() : &staticMetaObject;
}

void *SerialApp::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_SerialApp.stringdata))
        return static_cast<void*>(const_cast< SerialApp*>(this));
    return QWidget::qt_metacast(_clname);
}

int SerialApp::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 18)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 18;
    } else if (_c == QMetaObject::RegisterMethodArgumentMetaType) {
        if (_id < 18)
            *reinterpret_cast<int*>(_a[0]) = -1;
        _id -= 18;
    }
    return _id;
}

// SIGNAL 0
void SerialApp::showPlotButtonSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 0, 0);
}

// SIGNAL 1
void SerialApp::lineReceivedApp(QByteArray _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void SerialApp::closePortSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 2, 0);
}

// SIGNAL 3
void SerialApp::toggleTunerSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 3, 0);
}

// SIGNAL 4
void SerialApp::closeSignal()
{
    QMetaObject::activate(this, &staticMetaObject, 4, 0);
}
QT_END_MOC_NAMESPACE
