#ifndef MYSPINBOX_H
#define MYSPINBOX_H

#include <QSpinBox>
#include <QDoubleSpinBox>

class MySpinBox:public QSpinBox{
public:
  MySpinBox(){
    setObjectName("MySpinBox");
  }

  int id;
  QString name;
};

class MyDoubleSpinBox:public QDoubleSpinBox{
public:
  MyDoubleSpinBox(){
    setObjectName("MyDoubleSpinBox");
  }
  int id;
  QString name;
};

#endif // MYSPINBOX_H
