#include <cstdint>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <vector>
using namespace std;
int main(int argc, char* *argv)
{
	const string line = argv[argc - 1];
	vector<string> buf(26, string(128, ' '));
	for (int i = 0; i < 128 && i < line.size() / 2; ++i)
	{
		const uint8_t hex = stol(line.substr(i * 2, 2), nullptr, 16);
		buf[25 - hex / 10][i] = '+';
	}
	for (const string &s : buf)
	{
		cout << s << endl;
	}
}
